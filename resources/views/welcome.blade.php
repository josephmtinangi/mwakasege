<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Mana Tanzania</title>

    <!-- Fonts -->
    <link rel="stylesheet" type="text/css"
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body>

<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
                aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <a class="navbar-brand" href="#">Mana Tanzania</a>
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Kuhusu</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Shuhuda</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Maombi</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Mafundisho</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Gallery
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Picture</a>
                        <a class="dropdown-item" href="#">Video</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Mawasiliano</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Tafuta">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Tafuta</button>
            </form>
        </div>
    </div>
</nav>


<div class="container">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <img class="d-block img-fluid" src="http://placehold.it/1200x400" alt="First slide">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Gospel Service</h3>
                    <p>Contents...</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid" src="http://placehold.it/1200x400" alt="Second slide">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Prayer Service</h3>
                    <p>Contents...</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid" src="http://placehold.it/1200x400" alt="Third slide">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Charity Service</h3>
                    <p>Contents...</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

<div class="main">

    <section class="about">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 offset-sm-3 text-center">
                    <h1>Mana Tanzania</h1>
                    <p class="lead">
                        MANA ni Huduma ya Neno la Mungu yenye makao yake makuu Arusha Tanzania. Huduma hii inaongozwa na
                        Neno la
                        Mungu kutoka katika kitabu cha Yohana 21:17
                    </p>
                    <blockquote class="blockquote">
                        <p class="mb-0">Yesu akamwambia,"Lisha kondoo zangu".</p>
                        <footer class="blockquote-footer">Yohana 21:17</footer>
                    </blockquote>
                </div>
            </div>
        </div>
    </section>

    <section class="donate">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 offset-sm-3 text-center">
                    <h2>Changia Huduma</h2>
                    <p class="lead">
                        Karibu ushirikiane nasi katika kuchangia gharama za uendeshaji wa semina na Huduma nyingine
                        tunazofanya
                        maeneo mbalimbali.
                    </p>
                    <a href="#" class="btn btn-primary btn-lg">Changia</a>
                </div>
            </div>
        </div>
    </section>

    <section class="recent-events">
        <div class="container">
            <h2>Matukio ya hivi karibuni</h2>
            <div class="row">
                <div class="col-sm-4">
                    <div class="card" style="width: 20rem;">
                        <img class="card-img-top" src="http://placehold.it/300x250" alt="Card image cap">
                        <div class="card-block">
                            <h4 class="card-title">Semina ya Neno la Mungu</h4>
                            <small><i class="fa fa-map-marker"></i> Dar es Salaam</small>
                            <p class="card-text">Leo tarehe 29 Januari,tuna semina ukumbi wa Diamond
                                Jubilee, kuanzia saa saba mchana hadi saa 12 jioni, semina ya kupanda na kuvuna,
                                inayoandaliwa na huduma ya New Life In Christ Dar es Salaam.</p>
                            <span>
                                <a href="#" class="card-link"><i class="fa fa-file-pdf-o"></i></a>
                                <a href="#" class="card-link"><i class="fa fa-headphones"></i></a>
                                <a href="#" class="card-link"><i class="fa fa-film"></i></a>
                                <a href="#" class="card-link"><i class="fa fa-share"></i></a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card" style="width: 20rem;">
                        <img class="card-img-top" src="http://placehold.it/300x250" alt="Card image cap">
                        <div class="card-block">
                            <h4 class="card-title">Semina ya Neno la Mungu</h4>
                            <small><i class="fa fa-map-marker"></i> Dar es Salaam</small>
                            <p class="card-text">Leo tarehe 29 Januari,tuna semina ukumbi wa Diamond
                                Jubilee, kuanzia saa saba mchana hadi saa 12 jioni, semina ya kupanda na kuvuna,
                                inayoandaliwa na huduma ya New Life In Christ Dar es Salaam.</p>
                            <span>
                                <a href="#" class="card-link"><i class="fa fa-file-pdf-o"></i></a>
                                <a href="#" class="card-link"><i class="fa fa-headphones"></i></a>
                                <a href="#" class="card-link"><i class="fa fa-film"></i></a>
                                <a href="#" class="card-link"><i class="fa fa-share"></i></a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card" style="width: 20rem;">
                        <img class="card-img-top" src="http://placehold.it/300x250" alt="Card image cap">
                        <div class="card-block">
                            <h4 class="card-title">Semina ya Neno la Mungu</h4>
                            <small><i class="fa fa-map-marker"></i> Dar es Salaam</small>
                            <p class="card-text">Leo tarehe 29 Januari,tuna semina ukumbi wa Diamond
                                Jubilee, kuanzia saa saba mchana hadi saa 12 jioni, semina ya kupanda na kuvuna,
                                inayoandaliwa na huduma ya New Life In Christ Dar es Salaam.</p>
                            <span>
                                <a href="#" class="card-link"><i class="fa fa-file-pdf-o"></i></a>
                                <a href="#" class="card-link"><i class="fa fa-headphones"></i></a>
                                <a href="#" class="card-link"><i class="fa fa-film"></i></a>
                                <a href="#" class="card-link"><i class="fa fa-share"></i></a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col">
                <h3>Quick Links</h3>
                <ul class="list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Kuhusu Mana</a></li>
                    <li><a href="#">Nunua</a></li>
                    <li><a href="#">Mafundisho</a></li>
                    <li><a href="#">Maombi</a></li>
                    <li><a href="#">Ratiba</a></li>
                    <li><a href="#">Shuhuda</a></li>
                    <li><a href="#">Barua za mwezi</a></li>
                </ul>
            </div>
            <div class="col">
                <h3>Kuhusu</h3>
                <p>
                    MANA ni Huduma ya Neno la Mungu yenye makao yake makuu Arusha Tanzania. Huduma hii inaongozwa na
                    Neno la Mungu kutoka katika kitabu cha Yohana 21:17 "Yesu akamwambia,"Lisha kondoo zangu".
                </p>
            </div>
            <div class="col">
                <h3>Barua za wiki</h3>
                <form>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Barua pepe">
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-success btn-block">Jiunge</button>
                    </div>
                </form>
            </div>
            <div class="col">
                <h3>Mitandao ya kijamii</h3>
                <ul class="list-unstyled">
                    <li><a href="#"><i class="fa fa-facebook"></i> Facebook</a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i> Twitter</a></li>
                    <li><a href="#"><i class="fa fa-google-plus"> Google Plus</i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i> YouTube</a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i> Instagram</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="text-center">
        Haki zote zimehifadhiwa &copy; 2017 Huduma Ya Mana Tanzania. <a href="#">TERMS OF USE</a> | <a href="#">PRIVACY
            POLICY</a>
    </div>
</footer>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<script src="/js/app.js"></script>
</body>
</html>